<?php

namespace AppBundle\Controller\Api;
header('Access-Control-Allow-Origin: *');

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Api Article controller.
 */
class ApiArticleController extends FOSRestController implements ApiInterface {

    /**
     * Lister les articles
     * 
     * @ApiDoc(
     *  resource = true,
     *  description = "Lister les articles",
     *  section = "Articles",
     *  statusCodes = {
     *      200 = "Requête traitée avec succès",
     *      404 = "Ressource non trouvée"
     *  }
     * )
     * @Rest\Get("api/articles", name="api_articles", options={ "method_prefix" = false })
     */
    public function indexAction() {
        $apiManager = $this->get('article_api_manager');

        return $apiManager->processIndex();
    }

    /**
     * Ajouter un article
     * 
     * @ApiDoc(
     *  resource = true,
     *  description = "Ajouter un article",
     *  section = "Articles",
     *  statusCodes = {
     *      200 = "Requête traitée avec succès",
     *      404 = "Ressource non trouvée"
     *  }
     * )
     * @Rest\Post("api/articles", name="api_new_article", 
     * options={ "method_prefix" = false })
     */
    public function newAction() {
        $apiManager = $this->get('article_api_manager');

        return $apiManager->processCreateForm();
    }

    /**
     * Afficher un article selon son identifiant (id)
     * 
     * @ApiDoc(
     *  resource = true,
     *  description = "Afficher un article",
     *  section = "Articles",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="true",
     *          "description"="Identifiant de l'article",
     *      }
     *  },
     *  statusCodes = {
     *      200 = "Requête traitée avec succès",
     *      404 = "Ressource non trouvée"
     *  }
     * )
     * @Rest\Get("api/articles/{id}", name="api_article", 
     * requirements={"id" = "\d+"}, options={ "method_prefix" = false })
     */
    public function showAction($id) {
        $apiManager = $this->get('article_api_manager');

        return $apiManager->processShow($id);
    }

    /**
     * Modifier un article
     * 
     * @ApiDoc(
     *  resource = true,
     *  description = "Modifier un article",
     *  section = "Articles",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="true",
     *          "description"="Identifiant de l'article",
     *      }
     *  },
     *  statusCodes = {
     *      200 = "Requête traitée avec succès",
     *      404 = "Ressource non trouvée"
     *  }
     * )
     * @Rest\Put("api/articles/{id}", name="api_update_article", 
     * options={ "method_prefix" = false })
     */
    public function editAction($id) {
        $apiManager = $this->get('article_api_manager');

        return $apiManager->processEditForm($id);
    }

    /**
     * Supprimer un article
     * 
     * @ApiDoc(
     *  resource = true,
     *  description = "Supprimer un article",
     *  section = "Articles",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="true",
     *          "description"="Identifiant de l'article",
     *      }
     *  },
     *  statusCodes = {
     *      200 = "Requête traitée avec succès",
     *      404 = "Ressource non trouvée"
     *  }
     * )
     * @Rest\Delete("api/articles/{id}", name="api_delete_article", 
     * requirements={"id" = "\d+"}, options={ "method_prefix" = false })
     */
    public function deleteAction($id) {
        $apiManager = $this->get('article_api_manager');

        return $apiManager->processDelete($id);
    }

}
