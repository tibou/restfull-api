<?php

namespace AppBundle\Controller\Api;

/**
 * Interface ApiInterface
 */
interface ApiInterface {

    public function indexAction();

    public function newAction();

    public function showAction($id);

    public function editAction($id);

    public function deleteAction($id);
}
