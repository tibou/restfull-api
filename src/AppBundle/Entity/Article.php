<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Hateoas\Configuration\Annotation as Hateoas;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ArticleRepository")
 * @ExclusionPolicy("all")
 * @Hateoas\Relation("self", href = @Hateoas\Route("api_article", parameters = { "id" = "expr(object.getId())" }))
 * @Hateoas\Relation("delete", href = @Hateoas\Route("api_delete_article", parameters = { "id" = "expr(object.getId())" }))
 * @Hateoas\Relation("edit", href = @Hateoas\Route("api_update_article", parameters = { "id" = "expr(object.getId())" }))
 * @Hateoas\Relation("new", href = @Hateoas\Route("api_new_article"))
 * @Hateoas\Relation("index", href = @Hateoas\Route("api_articles"))))
 */
class Article {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotNull(
     *      message = "Ce champs ne peut être NULL")
     * @Assert\NotBlank(
     *      message = "Ce champs est obligatoire"
     * )
     * @Expose
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     * @Assert\NotNull(
     *      message = "Le contenu est obligatoire")
     * @Expose
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="summary", type="text")
     * @Assert\NotNull(
     *      message = "Le résumé est obligatoire")
     * @Expose
     */
    private $summary;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     * @Expose
     */
    private $enabled;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Article
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Article
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set summary
     *
     * @param string $summary
     * @return Article
     */
    public function setSummary($summary) {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get summary
     *
     * @return string 
     */
    public function getSummary() {
        return $this->summary;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Article
     */
    public function setEnabled($enabled) {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled() {
        return $this->enabled;
    }

}
