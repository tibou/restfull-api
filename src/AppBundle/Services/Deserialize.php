<?php

namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\RequestStack;
use JMS\Serializer\Serializer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Deserialize {

    private $format;
    private $class;
    private $request;
    private $serializer;
    private $validator;

    public function __construct($format, RequestStack $request_stack
    , Serializer $serializer, ValidatorInterface $validator) {
        
        $this->format = $format;
        $this->request = $request_stack->getCurrentRequest();
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    public function addClass($class) {
        return $this->class = $class;
    }

    public function deserialize() {

        try {
            $entity = $this->serializer->deserialize(
                    $this->request->getContent(), $this->class, $this->format
            );
        } catch (RuntimeException $e) {
            throw new HttpException(400, $e->getMessage());
        }

        if (count($errors = $this->validator->validate($entity))) {
            return $errors;
        }

        return $entity;
    }

}
