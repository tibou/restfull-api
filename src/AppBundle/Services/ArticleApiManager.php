<?php

namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Article;
use FOS\RestBundle\View\ViewHandler;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

class ArticleApiManager {

    private $request;
    private $em;
    private $repository;
    private $viewHandler;
    private $router;
    private $form;

    public function __construct(Request $request, EntityManager $em
    , ViewHandler $viewHandler, Router $router, Form $Form) {

        $this->request = $request;
        $this->em = $em;
        $this->viewHandler = $viewHandler;
        $this->router = $router;
        $this->form = $Form;
        $this->repository = $em->getRepository('AppBundle:Article');
    }

    public function setRepository($repository) {
        $this->repository = $repository;
    }

    public function processIndex() {
        $articles = $this->repository->findAll();
        
        return View::create($articles, '200');
    }

    public function processShow($id) {
        $article = $this->repository->find($id);
        if (!$article instanceof Article) {
            $data = array('documentaton' => $this->router->generate('nelmio_api_doc_index'),
                'errorCode' => '404', 'message' => 'Article non trouvé!');

            return View::create($data, '404');
        }

        return View::create($article, '200');
    }

    public function processDelete($id) {
        $article = $this->repository->find($id);
        if (!$article instanceof Article) {
            $data = array('documentaton' => $this->generateUrl('nelmio_api_doc_index'),
                'errorCode' => '404', 'message' => 'Article non trouvé!');

            return View::create($data, '404');
        }
        $this->em->remove($article);
        $this->em->flush();

        return View::create(NULL, '204');
    }

    public function processCreateForm() {
        $this->form->handleRequest($this->request);
        // check if $article is a new object or existant one
//        $statusCode = $this->em->contains($this->getForm()->getData()) ? 204 : 201;
        if ($this->form->isValid()) {
            $this->em->persist($this->getForm()->getData());
            $this->em->flush();

            $view = new View();
            $view->setStatusCode('201');
            // set the `Location` header only when creating new resources
            $url = $this->router->generate(
                    'api_article', array('id' => $this->getForm()->getData()->getId()), true
            );
            $view->setHeader('Location', $url);

            return $this->viewHandler->handle($view);
        }

        return View::create($this->form, 400);
    }

    public function processEditForm($id) {
        $article = $this->repository->find($id);
        if (!$article instanceof Article) {
            $data = array('documentaton' => $this->router->generate('nelmio_api_doc_index'),
                'errorCode' => '404', 'message' => 'Article non trouvé!');

            return View::create($data, '404');
        }
        $this->form->setData($article);
        $this->form->handleRequest($this->request);
        $this->form->submit($this->request);
        if ($this->form->isValid()) {
            $this->em->persist($this->getForm()->getData());
            $this->em->flush();

            return View::create(null, '204');
        }

        return View::create($this->form, 400);
    }

    public function getForm() {
        return $this->form;
    }

}
